package es.acore.abattery;

import java.util.*;

/**
 *
 * @author bruno
 */
public abstract class BatteryManager {

    public static BatteryManager createBM() {
        //variable de sistema
        String type = System.getProperty("batterymanager.impl");
        if ("db".equals(type)) {
            return new DatabaseBM();
        }
        if ("csv".equals(type)) {
            return new CsvfileBM();
        }
        throw new RuntimeException();
    }

    public abstract Battery getBatteryById(String batteryCode);

    public abstract List<Battery> list();

    public abstract void insert(Battery battery);

    public abstract void update(String batteryCode, Battery battery);

    public abstract void delete(String batteryCode);

    public abstract void cleanDatabase();
}
