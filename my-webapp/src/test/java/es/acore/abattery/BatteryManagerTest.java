package es.acore.abattery;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.Disabled;

/**
 *
 * @author yortx
 */
@Disabled("Not fixed in this branch")
public class BatteryManagerTest {

    private BatteryManager instance;

    @BeforeEach
    public void setUp() {
        CsvBatteryManagerTestHelper.cleanDatabase();
        System.setProperty("batterymanager.impl", "csv");
        instance = BatteryManager.createBM();
    }

    @Test
    public void testCreateBM() {
        assertNotNull(instance);
    }

    @Test
    public void testGetBatteryById() {
        Battery sampleBattery = sampleBattery();
        instance.insert(sampleBattery);

        Battery resultBattery = instance.getBatteryById(sampleBattery.getId());
        assertEquals(sampleBattery.getId(), resultBattery.getId());
        assertEquals(sampleBattery.getDescription(),
            resultBattery.getDescription());
        assertEquals(sampleBattery.getFecha(), resultBattery.getFecha());
    }

    @Test
    public void testGetBatteryByIdNotFound() {
        assertNull(instance.getBatteryById("nobattery"));
    }

    @Test
    public void testGetBatteryByIdFails() throws IOException {
        causeIOError();
        RuntimeException e = assertThrows(RuntimeException.class, () -> {
            instance.getBatteryById("nobattery");
        });
        assertNotNull(e.getCause());
        assertEquals(FileNotFoundException.class, e.getCause().getClass());
    }

    @Test
    public void testList() {
        Battery battery1 = sampleBattery();
        Battery battery2 = sampleBattery();
        battery2.setId("AA#02");

        instance.insert(battery1);
        instance.insert(battery2);

        List<Battery> batteryList = instance.list();
        assertEquals(2, batteryList.size());
        assertEquals(battery1.getId(), batteryList.get(0).getId());
        assertEquals(battery2.getId(), batteryList.get(1).getId());
    }

    @Test
    public void testListFailed() throws IOException {
        causeIOError();
        RuntimeException e = assertThrows(RuntimeException.class, () -> {
            instance.list();
        });
        assertNotNull(e.getCause());
        assertEquals(FileNotFoundException.class, e.getCause().getClass());
    }

    @Test
    public void testInsertFailed() throws IOException {
        causeIOError();
        RuntimeException e = assertThrows(RuntimeException.class, () -> {
            instance.insert(sampleBattery());
        });
        assertNotNull(e.getCause());
        assertEquals(FileNotFoundException.class, e.getCause().getClass());
    }

    @Test
    public void testUpdate() {
        Battery battery = sampleBattery();
        instance.insert(battery);
        String originalId = battery.getId();
        battery.setId("new-id");
        battery.setDescription("new description");
        battery.setFecha("1999-01-01");
        instance.update(originalId, battery);

        Battery resultBattery = instance.getBatteryById(battery.getId());
        assertEquals(battery.getId(), resultBattery.getId());
        assertEquals(battery.getDescription(), resultBattery.getDescription());
        assertEquals(battery.getFecha(), resultBattery.getFecha());
    }

    @Test
    public void testUpdateFailed() throws IOException {
        causeIOError();
        RuntimeException e = assertThrows(RuntimeException.class, () -> {
            instance.update("no-id", sampleBattery());
        });
        assertNotNull(e.getCause());
        assertEquals(FileNotFoundException.class, e.getCause().getClass());
    }

    @Test
    public void testDelete() {
        assertEquals(0, instance.list().size());
        Battery battery = sampleBattery();
        instance.insert(battery);
        assertEquals(1, instance.list().size());
        instance.delete(battery.getId());
        assertEquals(0, instance.list().size());
    }

    @Test
    public void testDeleteFailed() throws IOException {
        causeIOError();
        RuntimeException e = assertThrows(RuntimeException.class, () -> {
            instance.delete("no-id");
        });
        assertNotNull(e.getCause());
        assertEquals(FileNotFoundException.class, e.getCause().getClass());
    }

    private Battery sampleBattery() {
        Battery battery = new Battery();
        battery.setId("AA#01");
        battery.setDescription("Sample description");
        battery.setFecha("2022-12-31");
        return battery;
    }

    private void causeIOError() throws IOException {
        Files.deleteIfExists(Path.of(CsvBatteryManagerTestHelper.CSV_FILE));
        Files.createDirectory(Path.of(CsvBatteryManagerTestHelper.CSV_FILE));
    }
}
