package es.acore.abattery;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;

/**
 *
 * @author bruno
 */
@Controller
public class AddBatteryServlet {

    @GetMapping("AddBattery")
    public ModelAndView showForm() {
        ///Continuar desarrollando
        return new ModelAndView("newBatteryform", "battery", new Battery());
    }

}
