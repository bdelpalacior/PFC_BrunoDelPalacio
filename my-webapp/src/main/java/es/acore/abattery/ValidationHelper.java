package es.acore.abattery;

/**
 *
 * @author bruno
 */
public class ValidationHelper {

    public static boolean isNotEmpty(String cadena) {
        return cadena != null && !cadena.trim().equals("");
    }

    public static boolean isNumber(String cadena) {
        try {
            int numero = Integer.parseInt(cadena);
        } catch (NumberFormatException e) {
            return false;
        }
        return true;
    }

    public static boolean maxNumber(String cadena, long numero) {
        try {
            long numMenor = Long.parseLong(cadena);
            return numMenor <= numero;

        } catch (NumberFormatException e) {
            return false;
        }
    }

    public static boolean minNumber(String cadena, long numero) {
        try {
            long numMayor = Long.parseLong(cadena);
            return numero <= numMayor;

        } catch (NumberFormatException e) {
            return false;
        }
    }

    public static boolean isIn(String parametro, String... lista) {
        if (parametro == null) {
            return false;
        }
        if (lista == null) {
            return false;
        }
        for (String lista1 : lista) {
            if (parametro.equals(lista1)) {
                return true;
            }
        }
        return false;
    }

}
