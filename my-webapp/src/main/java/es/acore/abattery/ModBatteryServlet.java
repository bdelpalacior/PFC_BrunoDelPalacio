package es.acore.abattery;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestAttribute;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

/**
 *
 * @author bruno
 */
@Controller
public class ModBatteryServlet {

    @GetMapping("ModBattery")
    public ModelAndView showModForm(ModelMap model, @RequestParam("idToMod") String identificador) {

        Battery battery = BatteryManager.createBM().getBatteryById(identificador);

        model.addAttribute("idToMod", battery.getId());
        model.addAttribute("descriptionToMod", battery.getDescription());
        model.addAttribute("dateToMod", battery.getFecha());

        return new ModelAndView("modifyBatteryform", "battery", new Battery());
    }

    @PostMapping("ModBattery")
    public String submit(@ModelAttribute("battery") Battery battery, ModelMap model,
            RedirectAttributes attr, BindingResult result, @RequestParam("identificador") String identificador) {

        if (result.hasErrors()) {
            return "modelErrorPage";
        }

        BatteryManager.createBM().update(identificador, battery);

        attr.addFlashAttribute("id", battery.getId());
        attr.addFlashAttribute("description", battery.getDescription());
        attr.addFlashAttribute("fecha", battery.getFecha());

        return "redirect:/ModificationMessage";
    }

    @RequestMapping(value = "/ModificationMessage")
    public String showModificationMessage() {
        return "modificationMessage";
    }

}
