package es.acore.abattery;

import java.io.*;
import javax.servlet.*;
import javax.servlet.http.*;
import java.util.*;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

@Controller
public class MainServlet {

    @GetMapping("/")
    public String aBattery(ModelMap model) {

        List<String> erroresList;
        erroresList = new ArrayList<>();

        /*NOMBRE
        String nombre;
        nombre = request.getParameter("nombre");
        request.setAttribute("nombre", nombre);
        **/
        //SUBMIT
        if (erroresList.isEmpty()) {
            return "index";
            //request.getRequestDispatcher("/index.jspx").forward(request, response);

        } else {

            model.addAttribute("error", erroresList);
            return "index";
            //request.getRequestDispatcher("/index.jspx").forward(request, response);

        }
    }

    @PostMapping("/")
    public void doPost(HttpServletRequest request, HttpServletResponse response)
        throws ServletException, IOException {

        String profiles = System.getProperty("spring.profiles.active");
        if (profiles != null &&
            Arrays.asList(profiles.split(",")).contains("dev") &&
            "clean-database".equals(request.getParameter("dev-action"))) {

            BatteryManager.createBM().cleanDatabase();
        }
    }
}
