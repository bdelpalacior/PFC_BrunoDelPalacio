package es.acore.abattery;

import java.util.stream.Stream;
import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import static org.junit.jupiter.params.provider.Arguments.arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.junit.jupiter.params.provider.NullSource;
import org.junit.jupiter.params.provider.ValueSource;

/**
 *
 * @author bruno
 */
public class ValidationHelperTest {

    //isNotEmpty() Test
    //CORRECT
    static Stream<Arguments> testIsNotEmptyCorrect() {
        return Stream.of(
                arguments("123"),
                arguments("123 abc")
        );
    }

    @ParameterizedTest
    @MethodSource
    public void testIsNotEmptyCorrect(String text) {
        assertTrue(ValidationHelper.isNotEmpty(text));
    }

    //NOT CORRECT
    static Stream<Arguments> testIsNotEmptyNotCorrect() {
        return Stream.of(
                arguments((String) null),
                arguments(""),
                arguments("      "),
                arguments("\n\t")
        );
    }

    @ParameterizedTest
    @MethodSource
    public void testIsNotEmptyNotCorrect(String text) {
        assertFalse(ValidationHelper.isNotEmpty(text));
    }


    @ParameterizedTest
    @ValueSource(strings = {"123", "0"})
    public void testIsNumberCorrect(String text) {
        assertTrue(ValidationHelper.isNumber(text));
    }

    @ParameterizedTest
    @NullSource
    @ValueSource(strings = {
        " 123",
        "   ",
        "abc",
        "$5$",
        "3 4 h",
        "3,14",
        "3.14",
        "4 5 6"})
    public void testIsNumberNotCorrect(String text) {
        assertFalse(ValidationHelper.isNumber(text));
    }

    //maxNumber() Test
    //CORRECT
    static Stream<Arguments> testMaxNumberCorrect() {
        return Stream.of(
                arguments("1", 5),
                arguments("5", 5)
        );
    }

    @ParameterizedTest
    @MethodSource
    public void testMaxNumberCorrect(String text, long numero) {
        assertTrue(ValidationHelper.maxNumber(text, numero));
    }

    //NOT CORRECT
    static Stream<Arguments> testMaxNumberNotCorrect() {
        return Stream.of(
                arguments("2.5", 5),
                arguments("12", 5),
                arguments("2,5", 5),
                arguments("abc", 5),
                arguments("   ", 5),
                arguments(null, 1)
        );
    }

    @ParameterizedTest
    @MethodSource
    public void testMaxNumberNotCorrect(String text, long numero) {
        assertFalse(ValidationHelper.maxNumber(text, numero));
    }

    //minNumber() Test
    //CORRECT
    static Stream<Arguments> testMinNumberCorrect() {
        return Stream.of(
                arguments("5", 1),
                arguments("5", 5)
        );
    }

    @ParameterizedTest
    @MethodSource
    public void testMinNumberCorrect(String text, long numero) {
        assertTrue(ValidationHelper.minNumber(text, numero));
    }

    //NOT CORRECT
    static Stream<Arguments> testMinNumberNotCorrect() {
        return Stream.of(
                arguments("1", 5),
                arguments("2.5", 5),
                arguments("2,5", 5),
                arguments("abc", 5),
                arguments("   ", 5),
                arguments(null, 1)
        );
    }

    @ParameterizedTest
    @MethodSource
    public void testMinNumberNotCorrect(String text, long numero) {
        assertFalse(ValidationHelper.minNumber(text, numero));
    }

    //Test isIn()
    static Stream<Arguments> testIsInCorrect() {
        return Stream.of(
                arguments("apple", new String[]{"apple"}),
                arguments("apple", new String[]{new String("apple")}),
                arguments("apple", new String[]{"apple", "banana"})
        );
    }

    @ParameterizedTest
    @MethodSource
    public void testIsInCorrect(String text, String[] textList) {
    }

    static Stream<Arguments> testIsInNotCorrect() {
        return Stream.of(
                arguments(null, null),
                arguments("apple", null),
                arguments("apple", new String[]{}),
                arguments("apple", new String[]{"banana"})
        );
    }

    @ParameterizedTest
    @MethodSource
    public void testIsInNotCorrect(String text, String[] textList) {
        assertFalse(ValidationHelper.isIn(text, textList));
    }
}
