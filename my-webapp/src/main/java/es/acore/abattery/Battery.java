package es.acore.abattery;

/**
 *
 * @author bruno
 */
public class Battery {

    private int idInterno;
    private String description;
    private String id;
    private String fecha;
    private static int contador = 0;

    public Battery(String id, String description, String fecha) {
        this.id = id;
        this.description = description;
        this.fecha = fecha;
        this.idInterno = contador;
        contador++;

    }

    public Battery() {
        this.idInterno = contador;
        contador++;
    }

    /**
     * @return the id
     */
    public String getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * @param description the description to set
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * @return the fecha
     */
    public String getFecha() {
        return fecha;
    }

    /**
     * @param fecha the fecha to set
     */
    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

}
