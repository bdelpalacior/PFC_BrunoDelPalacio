/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package es.acore.abattery;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;

/**
 *
 * @author bruno
 */
public class DbManager {

    private static DbManager newDbManager = null;

    public static DbManager getInstance() throws SQLException {
        if (newDbManager == null) {
            newDbManager = new DbManager();
        }
        return newDbManager;
    }

    private DbManager() throws SQLException {

        PreparedStatement stmt = getConnection().prepareStatement("CREATE TABLE tablaPilas (id varchar(20) NOT NULL,description varchar(50),fecha varchar(20) NOT NULL)");
        stmt.execute();
        //Insertamos pilas
        PreparedStatement rows = getConnection().prepareStatement("INSERT INTO tablaPilas (id, description, fecha) VALUES ('AA#AA01','Pila AAA','2021-04-22')");
        rows.execute();
        //Aqui menejamos la bd
        stmt.close();
        rows.close();

    }

    public Connection getConnection() throws SQLException {
        String dbURL = "jdbc:derby:memory:myDB;create=true";

        Connection con = DriverManager.getConnection(dbURL);
        return con;
    }

}
