package es.acore.abattery;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Path;

public class CsvBatteryManagerTestHelper {

    public static final String CSV_FILE =
        "src/main/resources/PilasRecargables.csv";

    /**
     * Removes all data so we can start in a known state
     */
    public static void cleanDatabase() {
        Path path = Path.of(CsvBatteryManagerTestHelper.CSV_FILE);
        if (path.toFile().isDirectory()) {
            path.toFile().delete();
        }

        (new File("src/main/resources")).mkdirs();

        try ( FileWriter databaseFile
                = new FileWriter(CSV_FILE)) {

            databaseFile.write("");
        } catch (IOException ex) {
            throw new RuntimeException("Could not remove all data", ex);
        }
    }
}
