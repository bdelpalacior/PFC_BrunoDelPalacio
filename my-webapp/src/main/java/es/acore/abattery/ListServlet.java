package es.acore.abattery;

import java.io.*;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;

/**
 *
 * @author bruno
 */
@Controller
public class ListServlet {

    @GetMapping("Listado")
    public String listBatteries(ModelMap model)
            throws IOException {

        //Similar a lo que haciamos con la request
        model.addAttribute("pilas", BatteryManager.createBM().list());

        return "list";

    }
}
