# aBattery

## Descripción:

aBattery es en esencia una aplicación organizativa y de recuento de elementos, almacenando datos importantes sobre ellos y realizando diferentes funcionalidades la cuales conforman la aplicación.

## Instalación:

Recomiendo el uso de NetBeans como IDE, ya que con solo descargar la carpeta del proyecto y hacer "Build" sobre el, se descargarán las **dependencias** necesarias para que aBattery sea lanzada.

Para el uso de una base de datos, tras está estar implementada en nuestro sistema, se debe especificar en el archivo *DbManager.java (linea 40)*, dicho archivo controla nuestra BD.

## Modo de empleo:

A través de la IP 127.0.0.1 o localhost podrémos visualizar en nuestro navegador la vista de la aplicación, con la que podremos interactuar para ejecutar sus funcionalidades.
El funcionamiento de aBattery es sencillo e intuitivo.

## Licencia
**Apache License 2.0**
**aCore - Aplicacións Informáticas Corporativas e Empresariais, S.L.**




*Esta aplicación está libre de ningún requerimiento legal para su uso o difusión*

*Bruno del Palacio Rodríguez,          03/06/2022*
