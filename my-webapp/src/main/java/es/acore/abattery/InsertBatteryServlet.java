package es.acore.abattery;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

/**
 *
 * @author bruno
 */
@Controller
public class InsertBatteryServlet {

    @PostMapping("InsertBattery")
    public String submit(@ModelAttribute("battery") Battery battery, ModelMap model,
            RedirectAttributes attr, BindingResult result) {

        if (result.hasErrors()) {
            return "modelErrorPage";
        }

        attr.addFlashAttribute("nuevas", battery);
        BatteryManager.createBM().insert(battery);

        return "redirect:/Listado";

    }

}
