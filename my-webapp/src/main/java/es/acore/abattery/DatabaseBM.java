/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package es.acore.abattery;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author bruno
 */
public class DatabaseBM extends BatteryManager {

    @Override
    public Battery getBatteryById(String batteryCode) {
        List<Battery> pilas;
        pilas = new ArrayList<>();

        try {

            Statement stmt = DbManager.getInstance().getConnection().createStatement();
            ResultSet rs = stmt.executeQuery("SELECT id, description, fecha FROM tablaPilas WHERE id =" + batteryCode);

            Battery battery = new Battery();

            //Seteamos el id
            battery.setId(rs.getString("id"));
            //Seteamos la descripción
            battery.setDescription(rs.getString("description"));
            //Seteamos la fecha
            battery.setFecha(rs.getString("fecha"));

            pilas.add(battery);

            return battery;

        } catch (SQLException ex) {
            throw new RuntimeException();
        }

    }

    @Override
    public List<Battery> list() {

        List<Battery> pilas;
        pilas = new ArrayList<>();
        try {
            DbManager.getInstance();
        } catch (SQLException ex) {
            throw new RuntimeException();
        }
        try {
            Statement stmt = DbManager.getInstance().getConnection().createStatement();
            ResultSet rs = stmt.executeQuery("SELECT * FROM tablaPilas");

            while (rs.next()) {
                Battery battery = new Battery();

                //Seteamos el id
                battery.setId(rs.getString("id"));
                //Seteamos la descripción
                battery.setDescription(rs.getString("description"));
                //Seteamos la fecha
                battery.setFecha(rs.getString("fecha"));

                pilas.add(battery);
            }
        } catch (SQLException ex) {
            throw new RuntimeException();
        }

        return pilas;

    }

    @Override
    public void insert(Battery battery) {
        try {

            String id = battery.getId();
            String description = battery.getDescription();
            String fecha = battery.getFecha();
            try {
                DbManager.getInstance();
            } catch (SQLException ex) {
                throw new RuntimeException();
            }
            PreparedStatement stmt = DbManager.getInstance().getConnection().prepareStatement("INSERT INTO tablaPilas VALUES (?,?,?)");
            stmt.setString(1, id);
            stmt.setString(2, description);
            stmt.setString(3, fecha);
            stmt.executeUpdate();
            stmt.close();

        } catch (SQLException ex) {
            throw new RuntimeException();
        }
    }

    @Override
    public void update(String batteryCode, Battery battery) {
        try {

            String id = battery.getId();
            String description = battery.getDescription();
            String fecha = battery.getFecha();
            try {
                DbManager.getInstance();
            } catch (SQLException ex) {
                throw new RuntimeException();

            }
            PreparedStatement stmt;
            stmt = DbManager.getInstance().getConnection().prepareStatement("UPDATE tablaPilas SET id=?, description=?, fecha=? WHERE id=?");
            stmt.setString(1, id);
            stmt.setString(2, description);
            stmt.setString(3, fecha);
            stmt.setString(4, batteryCode);
            stmt.executeUpdate();
            stmt.close();

        } catch (SQLException ex) {
            throw new RuntimeException();
        }

    }

    @Override
    public void delete(String batteryCode) {
        try {
            DbManager.getInstance();
            PreparedStatement stmt = DbManager.getInstance().getConnection().prepareStatement("DELETE FROM tablaPilas WHERE id=?");
            stmt.setString(1, batteryCode);
            stmt.executeUpdate();
            stmt.close();

        } catch (SQLException ex) {
            throw new RuntimeException();
        }
    }

    @Override
    public void cleanDatabase() {
        executeInTransaction((connection) -> {
            connection.prepareStatement("DELETE FROM tablaPilas")
                    .executeUpdate();
        });
    }

    private void executeInTransaction(SqlRunnable runnable) {
        Connection connection = null;
        RuntimeException outerException = null;
        try {
            DbManager.getInstance();
            connection = DbManager.getInstance().getConnection();
            connection.setAutoCommit(false);
            runnable.executeSql(connection);
            connection.commit();
        } catch (SQLException | RuntimeException e) {
            outerException = e instanceof SQLException
                    ? new RuntimeException(e) : (RuntimeException) e;
            executeSupressed(outerException, connection, (c) -> {
                c.rollback();
            });
            throw outerException;
        } finally {
            executeSupressed(outerException, connection, (c) -> {
                c.close();
            });
        }
    }

    private void executeSupressed(RuntimeException outerException,
            Connection connection, SqlRunnable runnable) {

        if (connection == null) {
            return;
        }
        try {
            runnable.executeSql(connection);
        } catch (SQLException e) {
            if (outerException != null) {
                outerException.addSuppressed(outerException);
            } else {
                throw new RuntimeException(e);
            }
        }
    }

    private static interface SqlRunnable {

        public void executeSql(Connection connection) throws SQLException;
    }
}
