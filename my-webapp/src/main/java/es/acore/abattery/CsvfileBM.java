/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package es.acore.abattery;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author bruno
 */
public class CsvfileBM extends BatteryManager {

    public CsvfileBM() {

    }

    public static BatteryManager createBM() {
        return new CsvfileBM();

    }

    @Override
    public Battery getBatteryById(String batteryCode) {
        //Lee el fichero
        BufferedReader rd = null;
        try {
            rd = new BufferedReader(new FileReader("src/main/resources/PilasRecargables.csv"));
        } catch (Exception e) {
        }

        String line = null;
        try {
            line = rd.readLine();
        } catch (Exception e) {
        }

        //Por cada linea se ejecutará lo siguiente:
        while (line != null) {

            String[] linea = line.split(",");

            if (linea[0].trim().equals(batteryCode)) {
                Battery battery = new Battery();

                //Seteamos el id
                battery.setId(linea[0]);
                //Seteamos la descripción
                battery.setDescription(linea[1]);
                //Seteamos la fecha
                battery.setFecha(linea[2]);

                return battery;
            }

            try {
                line = rd.readLine();
            } catch (Exception e) {
            }

        }
        return null;
    }

    @Override
    public List<Battery> list() {
        //Lee el fichero
        BufferedReader rd = null;
        try {
            rd = new BufferedReader(new FileReader("src/main/resources/PilasRecargables.csv"));
        } catch (Exception e) {
        }

        String line = null;
        try {
            line = rd.readLine();
        } catch (Exception e) {
        }

        List<Battery> pilas;
        pilas = new ArrayList<>();

        //Por cada linea se ejecutará lo siguiente:
        while (line != null) {

            String[] linea = line.split(",");

            Battery pila = new Battery();

            //Seteamos el id
            pila.setId(linea[0]);
            //Seteamos la descripción
            pila.setDescription(linea[1]);
            //Seteamos la fecha
            pila.setFecha(linea[2]);

            pilas.add(pila);

            try {
                line = rd.readLine();
            } catch (Exception e) {
            }
        }

        return pilas;
    }

    @Override
    public void insert(Battery battery) {
        File fichero = new File("src/main/resources/PilasRecargables.csv");
        try ( FileWriter writer = new FileWriter("src/main/resources/PilasRecargables.csv", true)) {

            StringBuilder sb = new StringBuilder();
            if (fichero.length() != 0) {
                sb.append('\n');
            }
            sb.append(battery.getId());
            sb.append(',');
            sb.append(battery.getDescription());
            sb.append(',');
            sb.append(battery.getFecha());

            writer.write(sb.toString());

        } catch (Exception e) {
            System.out.println(e);
        }
    }

    @Override
    public void update(String batteryCode, Battery battery) {
        //Lee el fichero
        BufferedReader rd = null;
        try {
            rd = new BufferedReader(new FileReader("src/main/resources/PilasRecargables.csv"));
        } catch (Exception e) {
        }

        String line = null;
        try {
            line = rd.readLine();
        } catch (Exception e) {
        }

        List<String> pilas;
        pilas = new ArrayList<>();

        //Por cada linea se ejecutará lo siguiente:
        while (line != null) {

            String[] linea = line.split(",");

            if (!linea[0].trim().equals(batteryCode)) {

                pilas.add(line);
            } else {

                pilas.add(battery.getId() + "," + battery.getDescription() + "," + battery.getFecha());
            }

            try {
                line = rd.readLine();
            } catch (Exception e) {
            }

        }
        int i = 0;
        try ( //Escribimos el nuevo .csv
                 FileWriter fw = new FileWriter("src/main/resources/PilasRecargables.csv")) {
            for (String fila : pilas) {
                i++;
                fw.write(fila);
                if (i != pilas.size()) {
                    fw.write("\n");
                }

            }
        } catch (Exception e) {
        }
    }

    @Override
    public void delete(String batteryCode) {
        //Lee el fichero
        BufferedReader rd = null;
        try {
            rd = new BufferedReader(new FileReader("src/main/resources/PilasRecargables.csv"));
        } catch (Exception e) {
        }

        String line = null;
        try {
            line = rd.readLine();
        } catch (Exception e) {
        }

        List<String> pilas;
        pilas = new ArrayList<>();

        //Por cada linea se ejecutará lo siguiente:
        while (line != null) {

            String[] linea = line.split(",");

            if (!linea[0].trim().equals(batteryCode)) {

                pilas.add(line);
            }

            try {
                line = rd.readLine();
            } catch (Exception e) {
            }

        }
        int i = 0;
        try ( //Escribimos el nuevo .csv
                 FileWriter fw = new FileWriter("src/main/resources/PilasRecargables.csv")) {
            for (String fila : pilas) {
                i++;
                fw.write(fila);
                if (i != pilas.size()) {
                    fw.write("\n");
                }

            }
        } catch (Exception e) {
        }
    }

    @Override
    public void cleanDatabase() {
        try (FileWriter databaseFile =
            new FileWriter("src/main/resources/PilasRecargables.csv")) {

            databaseFile.write("");
        } catch (IOException ex) {
            throw new RuntimeException("Could not remove all data", ex);
        }
    }
}
