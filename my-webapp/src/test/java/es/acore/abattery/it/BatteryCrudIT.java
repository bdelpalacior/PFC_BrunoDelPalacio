package es.acore.abattery.it;

import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpRequest.BodyPublishers;
import java.net.http.HttpResponse.BodyHandlers;
import es.acore.abattery.CsvBatteryManagerTestHelper;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Objects;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

/**
 * Integration tests to CRUD functionality for Batteries.
 *
 * @author yortx
 */
public class BatteryCrudIT {

    private WebDriver webDriver;

    @BeforeEach
    public void setUp() {
        cleanDatabase();
        webDriver = new ChromeDriver();
    }

    @AfterEach
    public void tearDown() {
        webDriver.close();
    }

    /**
     * We should be able to navigate the app starting with the root url.
     */
    @Test
    public void testMain() {
        getRelative("/");
        assertEquals("aBattery",
                webDriver.findElement(By.tagName("h1")).getText());
    }

    /**
     * With an empty list, there should be some kind of message.
     */
    @Test
    public void testListEmpty() {
        BatteryListHelper listHelper = new BatteryListHelper(webDriver);
        assertEquals(0, listHelper.getNumBatteries());
        assertTrue(webDriver.getPageSource().contains("No hay pilas"));
    }

    /**
     * Adding a battery to an empty file should work.
     */
    @Test
    public void testAddBattery() {
        String sampleId = "AA#01";
        String sampleDescription = "My description";
        String sampleDate = "26/03/2022";

        BatteryListHelper listHelper = new BatteryListHelper(webDriver);
        listHelper.addBattery()
            .withId(sampleId)
            .withDescription(sampleDescription)
            .withDate(sampleDate)
            .save();

        assertEquals("Pila añadida recientemente",
            listHelper.getActionMessage());
        listHelper.assertNewBatteryData(
            sampleId, sampleDescription, sampleDate);
        assertEquals(1, listHelper.getNumBatteries());
        listHelper.assertRowBatteryData(
            1, sampleId, sampleDescription, sampleDate);
    }

    @Test
    public void testModifyFirstBattery() {
        BatteryListHelper listHelper = new BatteryListHelper(webDriver);
        listHelper.addBattery().withDefaultData().save();

        BatteryFormHelper batteryForm = listHelper.editBattery(1);
        assertEquals(BatteryFormHelper.SAMPLE_ID, batteryForm.getId());
        assertEquals(BatteryFormHelper.SAMPLE_DESCRIPTION, batteryForm.getDescription());
        assertEquals(LocalDate.parse(BatteryFormHelper.SAMPLE_DATE,
                DateTimeFormatter.ofPattern("dd/MM/yyyy")),
            LocalDate.parse(batteryForm.getDate()));

        String modifiedId = "AA#99";
        String modifiedDescription = "My modified description";
        String modifiedDate = "31/12/1999";

        batteryForm.withId(modifiedId)
            .withDescription(modifiedDescription)
            .withDate(modifiedDate)
            .save();

        assertTrue(webDriver.getPageSource()
                .contains("Nuevos datos de la pila"));
        assertEquals("ID: AA#99",
                webDriver.findElement(By.name("newId")).getText());
        assertEquals("Descripción: My modified description",
                webDriver.findElement(By.name("newDescription")).getText());
        assertEquals("Fecha de compra: 31/12/1999",
                webDriver.findElement(By.name("newFecha")).getText());

        listHelper.assertRowBatteryData(
            1, modifiedId, modifiedDescription, modifiedDate);
    }

    @Test
    public void testDelete() {
        BatteryListHelper listHelper = new BatteryListHelper(webDriver);
        listHelper.addBattery().withDefaultData().save();

        assertEquals(1, listHelper.getNumBatteries());

        listHelper.deleteBattery(1);

        assertEquals("aBattery",
                webDriver.findElement(By.tagName("h1")).getText());
        assertEquals("Se ha eliminado una pila de la lista:",
                webDriver.findElement(By.tagName("h2")).getText());

        assertEquals("ID: AA#01",
                webDriver.findElement(By.name("newId")).getText());
        assertEquals("Descripción: My description",
                webDriver.findElement(By.name("newDescription")).getText());
        assertEquals("Fecha de compra: 26/03/2022",
                webDriver.findElement(By.name("newFecha")).getText());

        assertEquals(0, listHelper.getNumBatteries());
    }

    private void getRelative(String relativeUrl) {
        Objects.nonNull(relativeUrl);

        StringBuilder absoluteUrl = new StringBuilder("http://localhost:8080");
        if (!relativeUrl.startsWith("/")) {
            absoluteUrl.append("/");
        }
        absoluteUrl.append(relativeUrl);
        webDriver.get(absoluteUrl.toString());
    }

    /** Removes all data so we can start in a known state */
    private void cleanDatabase() {
        try {
            //Map<Object, Object> data = new HashMap<>();
            //data.put("username", "abc");

            HttpClient httpClient = HttpClient.newHttpClient();
            HttpRequest request = HttpRequest.newBuilder()
                .uri(URI.create("http://localhost:8080/"))
                .POST(BodyPublishers.ofString("dev-action=clean-database"))
                .header("Content-Type", "application/x-www-form-urlencoded")
                .build();
            httpClient.send(request, BodyHandlers.discarding());
        } catch (IOException | InterruptedException ex) {
            throw new RuntimeException(ex);
        }
    }

    private static class BatteryListHelper {

        private final WebDriver webDriver;

        public BatteryListHelper(WebDriver webDriver) {
            this.webDriver = webDriver;
        }

        public BatteryFormHelper addBattery() {
            ensureListPage();
            webDriver.findElement(By.linkText("Añadir nueva pila")).click();
            return new BatteryFormHelper(webDriver);
        }

        public BatteryFormHelper editBattery(int rowNum) {
            WebElement row = getRow(rowNum);
            row.findElement(By.xpath(".//button[text()='Modificar pila']")).click();
            return new BatteryFormHelper(webDriver);
        }

        public void deleteBattery(int rowNum) {
            WebElement row = getRow(rowNum);
            row.findElement(By.xpath(".//button[text()='Borrar pila']")).click();
        }

        public int getNumBatteries() {
            ensureListPage();
            return webDriver.findElements(By.cssSelector("table tr")).size() - 1;
        }

        public String getActionMessage() {
            return webDriver.findElement(By.cssSelector("body > p")).getText();
        }

        public void assertNewBatteryData(
            String id, String description, String date) {

            assertEquals(id,
                webDriver.findElement(By.id("pilaNuevaId")).getText());
            assertEquals(description,
                webDriver.findElement(By.id("pilaNuevaDescription")).getText());
            assertEquals(date,
                webDriver.findElement(By.id("pilaNuevaDate")).getText());
        }

        public void assertRowBatteryData(
            int rowNum, String id, String description, String date) {

            ensureListPage();
            WebElement row = getRow(rowNum);
            assertEquals(id,
                row.findElement(By.cssSelector("td:nth-child(1)")).getText());
            assertEquals(description,
                row.findElement(By.cssSelector("td:nth-child(2)")).getText());
            assertEquals(date,
                row.findElement(By.cssSelector("td:nth-child(3)")).getText());
        }

        private void ensureListPage() {
            if (!isListPage()) {
                webDriver.get("http://localhost:8080/");
                webDriver.findElement(By.linkText("Lista de pilas")).click();
            }
            assertTrue(isListPage());
        }

        private boolean isListPage() {
            try {
                return webDriver.findElement(By.tagName("h2")).getText()
                        .equals("Lista de pilas") &&
                    webDriver.findElements(By.id("battery-list")).size() == 1;
            } catch (NoSuchElementException e) {
                return false;
            }
        }

        private WebElement getRow(int rowNum) {
            return webDriver.findElement(By.cssSelector(
                "table tr:nth-child(" + (rowNum + 1) + ")"));
        }
    }

    private static class BatteryFormHelper {

        public static final String SAMPLE_ID = "AA#01";
        public static final String SAMPLE_DESCRIPTION = "My description";
        public static final String SAMPLE_DATE = "26/03/2022";

        private final WebDriver webDriver;

        public BatteryFormHelper(WebDriver webDriver) {
            this.webDriver = webDriver;
        }

        public BatteryFormHelper withId(String id) {
            writeInput(webDriver.findElement(By.name("id")), id);
            return this;
        }

        public BatteryFormHelper withDescription(String description) {
            writeInput(webDriver.findElement(By.name("description")), description);
            return this;
        }

        public BatteryFormHelper withDate(String date) {
            writeInput(webDriver.findElement(By.name("fecha")), date);
            return this;
        }

        public BatteryFormHelper withDefaultData() {
            withId(SAMPLE_ID);
            withDescription(SAMPLE_DESCRIPTION);
            withDate(SAMPLE_DATE);
            return this;
        }

        public String getId() {
            return webDriver.findElement(By.name("id")).getAttribute("value");
        }

        public String getDescription() {
            return webDriver.findElement(By.name("description")).getAttribute("value");
        }

        public String getDate() {
            return webDriver.findElement(By.name("fecha")).getAttribute("value");
        }

        public void save() {
            webDriver.findElement(By.cssSelector("input[value=Submit]")).click();
        }

        private void writeInput(WebElement input, String value) {
            input.clear();
            input.sendKeys(value);
        }
    }
}
