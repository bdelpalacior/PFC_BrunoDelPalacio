/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/UnitTests/JUnit4TestClass.java to edit this template
 */
package es.acore.abattery.it;

import es.acore.abattery.Battery;
import es.acore.abattery.BatteryManager;
import es.acore.abattery.DatabaseBM;
import es.acore.abattery.DbManager;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

/**
 *
 * @author bruno
 */
public class DatabaseBMTest {

    private DatabaseBM batteryManager;

    @BeforeEach
    public void setUp() {
        batteryManager = new DatabaseBM();
        batteryManager.cleanDatabase();
    }

    //getBatteryById() Test
    @Test
    public void testGetBatteryByIdBD() throws SQLException {
        System.out.println("testGetBatteryById");
        //Creación de Entorno controlado
        Connection con = DbManager.getInstance().getConnection();
        Statement stmtTest = con.createStatement();
        PreparedStatement rowsTest = con.prepareStatement("INSERT INTO tablaPilas (id, description, fecha) VALUES ('AA#AA01','Pila AAA','20-04-2021')");
        rowsTest.execute();
        stmtTest.close();
        rowsTest.close();

        BatteryManager bm = new DatabaseBM();
        Battery pilaPrueba = new Battery("AA#AA01", "Pila AAA", "20-04-2021");

        //Funcionalidad del metodo
        Battery pilaBuscada = bm.getBatteryById("AA#AA01");
        assertEquals(pilaBuscada.getId(), pilaPrueba.getId());
        assertEquals(pilaBuscada.getDescription(), pilaPrueba.getDescription());
        assertEquals(pilaBuscada.getFecha(), pilaPrueba.getFecha());

        PreparedStatement dropTable = DbManager.getInstance().getConnection().prepareStatement("DROP TABLE tablaPilas");

    }

    //list() Test
    @Test
    public void testListBD() throws SQLException {
        System.out.println("testList");
        //Creación de Entorno controlado
        Statement stmtTest = DbManager.getInstance().getConnection().createStatement();
        PreparedStatement rowsTest = DbManager.getInstance().getConnection().prepareStatement("INSERT INTO tablaPilas (id, description, fecha) VALUES ('AA#AA01','Pila AAA','20-04-2021')");
        rowsTest.execute();
        stmtTest.close();
        rowsTest.close();

        BatteryManager bm = new DatabaseBM();
        Battery pilaPrueba = new Battery("AA#AA01", "Pila AAA", "20-04-2021");
        List<Battery> lista = new ArrayList<>();
        lista.add(pilaPrueba);

        //Funcionalidad del metodo
        List<Battery> listaDevuelta = bm.list();

        assertEquals(lista.get(0).getId(), listaDevuelta.get(0).getId());
        assertEquals(lista.get(0).getDescription(), listaDevuelta.get(0).getDescription());
        assertEquals(lista.get(0).getFecha(), listaDevuelta.get(0).getFecha());

        PreparedStatement dropTable = DbManager.getInstance().getConnection().prepareStatement("DROP TABLE tablaPilas");

    }

    //insert() Test
    @Test
    public void testInsertBD() throws SQLException {
        BatteryManager bm = new DatabaseBM();
        Battery pilaPrueba = new Battery("AA#AA01", "Pila AAA", "20-04-2021");

        //Funcionalidad del metodo
        bm.insert(pilaPrueba);
        List<Battery> listaDevuelta = bm.list();
        assertEquals("AA#AA01", listaDevuelta.get(0).getId());
        assertEquals("Pila AAA", listaDevuelta.get(0).getDescription());
        assertEquals("20-04-2021", listaDevuelta.get(0).getFecha());

    }

    //update() Test
    @Test
    public void testUpdateBD() throws SQLException {
        System.out.println("testUpdate");
        //Creación de Entorno controlado
        Statement stmtTest = DbManager.getInstance().getConnection().createStatement();
        stmtTest.close();

        Battery pilaPrueba = new Battery("AA#AA01", "Pila AAA", "20-04-2021");
        BatteryManager bm = new DatabaseBM();
        bm.insert(pilaPrueba);

        //Funcionalidad del metodo
        Battery pilaModificada = new Battery("AA#AA02", "Pila AAA 2", "20-04-2022");

        bm.update(pilaPrueba.getId(), pilaModificada);
        List<Battery> listaDevuelta = bm.list();
        assertEquals("AA#AA02", listaDevuelta.get(0).getId());
        assertEquals("Pila AAA 2", listaDevuelta.get(0).getDescription());
        assertEquals("20-04-2022", listaDevuelta.get(0).getFecha());

    }

    //delete() Test
    @Test
    public void testDeleteBD() throws SQLException {
        System.out.println("testDelete");
        //Creación de Entorno controlado
        Statement stmtTest = DbManager.getInstance().getConnection().createStatement();

        stmtTest.close();

        Battery pilaPrueba = new Battery("AA#AA01", "Pila AAA", "20-04-2021");
        BatteryManager bm = new DatabaseBM();
        bm.insert(pilaPrueba);

        //Funcionalidad del metodo
        bm.delete(pilaPrueba.getId());
        List<Battery> listaDevuelta = bm.list();
        assertTrue(listaDevuelta.isEmpty());
    }

}
