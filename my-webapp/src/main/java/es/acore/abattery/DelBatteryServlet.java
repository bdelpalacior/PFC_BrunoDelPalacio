package es.acore.abattery;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

/**
 *
 * @author bruno
 */
@Controller
public class DelBatteryServlet {

    @PostMapping("DelBattery")
    public String deleteBattery(ModelMap model, @RequestParam("identificador") String identificador, RedirectAttributes attr) {

        Battery pilaBorrada = BatteryManager.createBM().getBatteryById(identificador);

        BatteryManager.createBM().delete(identificador);

        attr.addFlashAttribute("id", pilaBorrada.getId());
        attr.addFlashAttribute("description", pilaBorrada.getDescription());
        attr.addFlashAttribute("fecha", pilaBorrada.getFecha());

        return "redirect:/DelBattery";
    }

    @GetMapping("DelBattery")
    public String deletionMessage() {

        return "deletionMessage";
    }

}
